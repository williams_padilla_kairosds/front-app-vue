FROM node:16-alpine3.15 AS builder

WORKDIR /front-app

COPY . .

RUN npm install

RUN npm run build

FROM nginx:1.23.1-alpine

COPY --from=builder /front-app/dist /usr/share/nginx/html/front-app

RUN rm /etc/nginx/conf.d/default.conf

COPY deploy/nginx/nginx.conf /etc/nginx/conf.d/

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]