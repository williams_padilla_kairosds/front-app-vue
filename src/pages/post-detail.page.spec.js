/**
 * @vitest-environment happy-dom
 */

import { mount } from "@vue/test-utils";
import { describe, expect, it } from "vitest";
import PostDetailUI from "./../ui/post-detail.ui.vue";
import PostDetail from "./post-detail.page.vue";
describe("Post detail page", () => {
    it("should render", () => {
        const wrapper = mount(PostDetail, {
            global: {
                mocks: {
                    $route: { params: { id: "" } },
                },
            },
        });
        const UI = mount(PostDetailUI, {
            propsData: {
                post: {
                    id: "7975a840-a042-485b-ba11-e170536b52a1",
                    author: "StarFlow",
                    title: "Como estas",
                    text: "Lorem ipsum dolor sit amet, consectetuer adipiscing",
                    comments: [
                        {
                            id: "bf5c26d6-9d38-4a80-8bab-6fb5e39ce6e6",
                            authorId: "d175e508-55f2-4f39-8f10-0684048e9986",
                            author: "GodAdmin",
                            content: "soy un comentario de user2",
                        },
                        {
                            id: "bf5c26d6-9d38-4a80-8bab-6fb5e39ce6e6",
                            authorId: "d175e508-55f2-4f39-8f10-0684048e9986",
                            author: "GodAdmin",
                            content: "soy un comentario de user3",
                        },
                    ],
                },
            },
        });

        expect(wrapper.exists()).toBeTruthy();
        expect(UI.props().post.author).toBe("StarFlow");
    });
});
