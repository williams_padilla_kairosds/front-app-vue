/**
 * @vitest-environment happy-dom
 */

import { mount } from "@vue/test-utils";
import { describe, expect, it } from "vitest";
import PostUI from "./../ui/post-ui.vue";
import PostPage from "./posts.page.vue";

describe("Post page component", () => {
    it("should render", () => {
        const wrapper = mount(PostPage);
        const postUI = mount(PostUI);

        wrapper.findAllComponents(postUI).forEach((post) => {
            expect(post.exists()).toBeTruthy();
        });
    });
});
