import axios from "axios";
import environment from "../services/config/config.service";

export class PostRepository {
    async getPosts() {
        const response = await axios.get(`${environment.config}post/public/`);
        return response.data;
    }

    async getComments(postId) {
        const url = `${environment.config}post/${postId}`;
        console.log("url del get by id", url);

        const response = await axios.get(`${environment.config}post/${postId}`);
        console.log("response", response.data);
        return response.data;
    }
}
