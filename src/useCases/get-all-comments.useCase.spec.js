vi.mock("./../repositories/post.repository.js", () => {
    return {
        PostRepository: vi.fn().mockImplementation(() => {
            return {
                getComments: vi.fn().mockImplementation(() => {
                    return {
                        id: "7975a840-a042-485b-ba11-e170536b52a1",
                        author: "StarFlow",
                        title: "Como estas",
                        text: "Lorem ipsum dolor sit amet, consectetuer adipiscing",
                        comments: [
                            {
                                id: "bf5c26d6-9d38-4a80-8bab-6fb5e39ce6e6",
                                authorId:
                                    "d175e508-55f2-4f39-8f10-0684048e9986",
                                author: "GodAdmin",
                                content: "soy un comentario de user2",
                            },
                            {
                                id: "bf5c26d6-9d38-4a80-8bab-6fb5e39ce6e6",
                                authorId:
                                    "d175e508-55f2-4f39-8f10-0684048e9986",
                                author: "GodAdmin",
                                content: "soy un comentario de user3",
                            },
                        ],
                    };
                }),
            };
        }),
    };
});

import { describe, expect, it, vi } from "vitest";
import { GetAllCommentsFromPostUseCase } from "./get-all-comments.useCase";
describe("Get all public posts useCase", () => {
    it("should return a list of public posts", async () => {
        const useCase = new GetAllCommentsFromPostUseCase();
        const post = await useCase.execute();

        // expect(post).toHaveLength(2);
        expect(post.author).toBe("StarFlow");
        expect(post.comments[1].content).toBe("soy un comentario de user3");
        expect(post.comments).toHaveLength(2);
    });
});
