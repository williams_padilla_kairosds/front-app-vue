import { PostRepository } from "../repositories/post.repository";

export class GetAllCommentsFromPostUseCase {
    async execute(postId) {
        const repository = new PostRepository();
        const postInfo = await repository.getComments(postId);
        return postInfo;
    }
}
