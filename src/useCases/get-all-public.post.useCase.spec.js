vi.mock("../repositories/post.repository.js", () => {
    return {
        PostRepository: vi.fn().mockImplementation(() => {
            return {
                getPosts: vi.fn().mockImplementation(() => {
                    return [
                        {
                            id: "7975a840-a042-485b-ba11-e170536b52a1",
                            author: "StarFlow",
                            title: "Como estas",
                            text: "Lorem ipsum dolor sit amet, consectetuer adipiscing",
                        },
                        {
                            id: "d41dcceb-d51f-4eaa-bca9-580e3d3af177",
                            author: "Vanjie",
                            title: "It's venjie!!!",
                            text: "Im not from spain, this is probably my first coment ever, so pleaso dont be mean\n",
                        },
                    ];
                }),
            };
        }),
    };
});

import { describe, expect, it, vi } from "vitest";
import { GetAllPublicPostsUseCase } from "./get-all-public-posts.useCase";
describe("Get all public posts useCase", () => {
    it("should return a list of public posts", async () => {
        const useCase = new GetAllPublicPostsUseCase();
        const posts = await useCase.execute();

        expect(posts).toHaveLength(2);
        expect(posts[0].author).toBe("StarFlow");
    });
});
