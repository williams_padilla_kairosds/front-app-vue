import { PostRepository } from "../repositories/post.repository";

export class GetAllPublicPostsUseCase {
    async execute() {
        const repository = new PostRepository();
        const allPost = await repository.getPosts();
        return allPost;
    }
}
