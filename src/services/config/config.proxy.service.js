import axios from "axios";

export class ConfigProxyService {
    async getConfig() {
        const response = await axios.get("/front-app/config/config.json");
        return response.data;
    }
}
