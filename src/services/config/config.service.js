import { ConfigProxyService } from "./config.proxy.service";

let api = "";

export class ConfigService {
    get config() {
        return api;
    }

    async load() {
        const service = new ConfigProxyService();
        const config = await service.getConfig();
        console.log("url de Load()", config.api);
        api = config.api;
    }
}
const serviceInstanceSingleton = new ConfigService();
Object.freeze(serviceInstanceSingleton);
export default serviceInstanceSingleton;
