import { createApp } from "vue";
import { createRouter, createWebHistory } from "vue-router";
import App from "./App.vue";
import PostDetail from "./pages/post-detail.page.vue";
import PostPage from "./pages/posts.page.vue";
import initConfig from "./services/config/config.service";
import "./style.css";

const router = createRouter({
    history: createWebHistory("/front-app"),
    routes: [
        { path: "/", name: "posts", component: PostPage },
        { path: "/post/:id", name: "postComments", component: PostDetail },
    ],
});

initConfig.load().then(() => {
    createApp(App).use(router).mount("#app");
});

const init = async () => {
    return await initConfig.load();
};
